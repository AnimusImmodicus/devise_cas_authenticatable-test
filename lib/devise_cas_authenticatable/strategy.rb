require 'devise/strategies/base'
require 'devise/strategies/authenticatable'

module Devise
  module Strategies
    class CasAuthenticatable < Authenticatable
      # True if the mapping supports authenticate_with_cas_ticket.
      def valid?
        puts "\n\nChecking validity for TestingCasAuthenticatable\n\n"
        puts "\n#{mapping.inspect}\n"
        # puts "\n#{mapping.to.respond_to?(:authenticate_with_cas_ticket)}\n\n"
        # puts "\n#{params[:ticket]}"
        # return true
        # return true
        mapping.to.respond_to?(:authenticate_with_cas_ticket) && params[:ticket]
      end

      # Try to authenticate a user using the CAS ticket passed in params.
      # If the ticket is valid and the model's authenticate_with_cas_ticket method
      # returns a user, then return success.  If the ticket is invalid, then either
      # fail (if we're just returning from the CAS server, based on the referrer)
      # or attempt to redirect to the CAS server's login URL.
      def authenticate!
        puts "\nTesting CasAuthenticatable authentication\n"

        ticket = read_ticket(params)
        if ticket
          if resource = mapping.to.authenticate_with_cas_ticket(ticket)
            # Store the ticket in the session for later usage
            if ::Devise.cas_enable_single_sign_out
              session['cas_last_valid_ticket'] = ticket.ticket
              session['cas_last_valid_ticket_store'] = true
            end

            puts "\n\nSUCCESS!\n"

            success!(resource)
          elsif ticket.is_valid?
            username = ticket.respond_to?(:user) ? ticket.user : ticket.response.user
            redirect!(::Devise.cas_unregistered_url(request.url, mapping), :username => username)
          else
            puts "\n\nFAIL\n"
            fail!(:invalid)
          end
        else
          puts "\n\nFail else\n"
          puts ::Devise.cas_login_url
          fail!(:invalid)
        end
      end

      protected

      def read_ticket(params)
        ticket = params[:ticket]
        return nil unless ticket

        service_url = ::Devise.cas_service_url(request.url, mapping)
        if ticket =~ /^PT-/
          ::CASClient::ProxyTicket.new(ticket, service_url, params[:renew])
        else
          ::CASClient::ServiceTicket.new(ticket, service_url, params[:renew])
        end
      end
    end
  end
end

puts "ADD STRATEGYT\n\n\n"
Warden::Strategies.clear!
puts "  CLEAJSDFJKLASD\n\n\n"
Warden::Strategies.add(:cas_authenticatable, Devise::Strategies::CasAuthenticatable)
