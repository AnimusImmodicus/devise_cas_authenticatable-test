# Redirect to the logout url when :warden is thrown,
# so that a single_sign_out request can be initiated
class DeviseCasAuthenticatable::SingleSignOut::WardenFailureApp < Devise::FailureApp

  # You need to override respond to eliminate recall
  def respond
    if http_auth?
      http_auth
    else
      redirect
    end
  end

  def redirect
    store_location!
    if flash[:timedout] && flash[:alert]
      flash.keep(:timedout)
      flash.keep(:alert)
    else
      flash[:alert] = i18n_message
    end
    redirect_to redirect_url
  end

  protected

  def redirect_url
    if [:timeout, :inactive].include? warden_message
      Rails.logger.info "WARDEN TIMEOUT REDIRECT_URL BEING USED"
      flash[:timedout] = true if warden_message == :timeout
      Devise.cas_client.logout_url
    else
      if respond_to?(:scope_path)
        Rails.logger.info "SCOPED REDIRECT URL BEING USED NOW"
        scope_path
      else
        # super
        puts "REDIRECT_URL BEING USED NOW"
        # Rails.logger.info "#{request.inspect}"
        # "#{Devise.cas_base_url}/login?service=#{request.original_url}"
        my_rurl = "#{Devise.cas_base_url}/login?service=#{::Devise.cas_service_url(request.original_url, Devise.mappings[:user])}&destination=#{request.original_url}"
        puts "#{my_rurl}"
        return my_rurl
      end
    end
  end

  # Devise < 2.0 doesn't have this method, which we want to use
  unless instance_methods.include?(:warden_message)
    define_method :warden_message do
      @message ||= warden.message || warden_options[:message]
    end
  end
end
